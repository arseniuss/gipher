package lv.id.arseniuss.gipher.model.service;

import java.io.IOException;

import javax.inject.Inject;

import lv.id.arseniuss.gipher.model.preference.PreferenceHelper;
import lv.id.arseniuss.gipher.model.service.entity.GifSearchResponse;
import lv.id.arseniuss.gipher.model.service.entity.ServiceInterface;
import lv.id.arseniuss.gipher.utils.Constants;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class DataAccessImpl implements DataAccess {
    private final PreferenceHelper<String> stringPreferences;
    ServiceInterface serviceInterface;
    OkHttpClient httpClient;

    @Inject
    public DataAccessImpl(OkHttpClient httpClient, Retrofit retrofit, PreferenceHelper<String> stringPreferences) {
        this.serviceInterface = retrofit.create(ServiceInterface.class);
        this.stringPreferences = stringPreferences;
        this.httpClient = httpClient;
    }

    @Override
    public Observable<GifSearchResponse> searchGif(String query, int limit, int offset) {
        return stringPreferences.get(Constants.GIPHY_API_KEY_TOKEN, String.class)
                .flatMap(s -> serviceInterface.gifsSearch(s, query, limit, offset))
                .compose(o ->
                        o.subscribeOn(Schedulers.io())
                                .unsubscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                );
    }

    @Override
    public Observable<byte[]> downloadGif(String url) {
        return Observable.just(url)
                .flatMap(u -> {
                    try {
                        Response response = httpClient.newCall(
                                new Request.Builder()
                                        .url(u).build())
                                .execute();

                        return Observable.just(response.body().bytes());
                    } catch (IOException ex) {
                        return Observable.error(ex);
                    }
                })
                .compose(o ->
                        o.subscribeOn(Schedulers.io())
                                .unsubscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                );
    }
}
