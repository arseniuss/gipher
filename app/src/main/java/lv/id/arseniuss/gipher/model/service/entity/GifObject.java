package lv.id.arseniuss.gipher.model.service.entity;

public class GifObject {
    public String type;
    public String id;
    public String slug;
    public String url;
    public String bitly_url;
    public String embed_url;
    public String username;
    public String source;
    public String rating;
    public String content_url;
    public UserObject user;
    public String source_tld;
    public String source_post_url;
    public String update_datetime;
    public String create_datetime;
    public String import_datetime;
    public String trending_datetime;
    public ImagesObject images;
    public String title;
}
