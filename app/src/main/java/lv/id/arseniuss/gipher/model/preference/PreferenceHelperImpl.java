package lv.id.arseniuss.gipher.model.preference;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Inject;

import rx.Observable;

public class PreferenceHelperImpl<T> implements PreferenceHelper<T> {

    private final SharedPreferences sharedPreferences;
    private final Gson gson;

    @Inject
    public PreferenceHelperImpl(SharedPreferences sharedPreferences, Gson gson) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
    }

    @Override
    public Observable<T> save(String key, T value) {
        return Observable.unsafeCreate(s -> {
           sharedPreferences.edit().putString(key, gson.toJson(value)).apply();
           s.onNext(value);
           s.onCompleted();
        });
    }

    @Override
    public Observable<T> get(String key, Class<T> aClass) {
        return Observable.unsafeCreate(s -> {
            String json = sharedPreferences.getString(key, "");
            T theClass = gson.fromJson(json, aClass);
            s.onNext(theClass);
            s.onCompleted();
        });
    }

    @Override
    public Observable<Boolean> clear() {
        return Observable.unsafeCreate(s -> {
           sharedPreferences.edit().clear().commit();
           s.onNext(true);
           s.onCompleted();
        });
    }
}
