package lv.id.arseniuss.gipher.model.service.entity;

import android.support.annotation.NonNull;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;


public interface ServiceInterface {

    /**
     * Full API request
     * API documentation: https://developers.giphy.com/docs/#operation--gifs-search-get
     */
    @GET("v1/gifs/search")
    Observable<GifSearchResponse> gifsSearch(
            @Query("api_key") @NonNull String api_key,
            @Query("q") @NonNull String q,
            @Query("limit") Integer limit,
            @Query("offset") Integer offset,
            @Query("rating") String rating,
            @Query("lang") String lang,
            @Query("fmt") String fmt
    );

    /**
     * Minimal API request
     * API documentation: https://developers.giphy.com/docs/#operation--gifs-search-get
     */
    @GET("v1/gifs/search")
    Observable<GifSearchResponse> gifsSearch(
            @Query("api_key") @NonNull String api_key,
            @Query("q") @NonNull String q,
            @Query("limit") Integer limit,
            @Query("offset") Integer offset);
}
