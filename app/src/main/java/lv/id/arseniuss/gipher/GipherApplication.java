package lv.id.arseniuss.gipher;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import lv.id.arseniuss.gipher.di.NetworkModule;
import lv.id.arseniuss.gipher.di.app.AppComponent;
import lv.id.arseniuss.gipher.di.app.AppModule;
import lv.id.arseniuss.gipher.di.app.DaggerAppComponent;
import lv.id.arseniuss.gipher.di.main.DaggerMainComponent;
import lv.id.arseniuss.gipher.di.main.MainComponent;
import lv.id.arseniuss.gipher.di.main.MainModule;
import lv.id.arseniuss.gipher.utils.Constants;

public class GipherApplication extends Application {
    private AppComponent appComponent;
    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initInject();
        checkFirstRun();
    }

    private void checkFirstRun() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        if(sharedPreferences.getBoolean("firstrun", true)) {
            sharedPreferences.edit()
                    .putBoolean("firstrun", false)
                    .putString(Constants.GIPHY_API_KEY_TOKEN, BuildConfig.giphy_api_key)
                    .commit();
        }
    }

    private void initInject() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .build();

        mainComponent = DaggerMainComponent.builder()
                .appComponent(appComponent)
                .mainModule(new MainModule())
                .networkModule(new NetworkModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
