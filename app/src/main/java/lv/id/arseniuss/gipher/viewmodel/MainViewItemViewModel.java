package lv.id.arseniuss.gipher.viewmodel;

import android.databinding.ObservableField;
import android.view.View;

import lv.id.arseniuss.gipher.model.service.DataAccess;
import lv.id.arseniuss.gipher.model.service.entity.GifObject;

public class MainViewItemViewModel {
    private GifObject gif;
    private DataAccess dataAccess;
    private Listener listener;
    private Boolean isAnimating = false;

    public ObservableField<String> title;
    public ObservableField<byte[]> bytes;
    public ObservableField<Integer> visibility;
    public int width;
    public int height;

    public MainViewItemViewModel(DataAccess dataAccess, Listener listener, GifObject gif, int cellWidth) {
        this.gif = gif;
        this.dataAccess = dataAccess;
        this.listener = listener;

        title = new ObservableField<>(gif.title);
        bytes = new ObservableField<>((byte[]) null);
        // since gifs are async, it's better to hide transition
        visibility = new ObservableField<>(View.INVISIBLE);

        // (w/h) = (w1/h1); h1 = (w1 * h) /w;

        int imageWidth = Integer.parseInt(gif.images.fixed_width.width);
        int imageHeight = Integer.parseInt(gif.images.fixed_width.height);

        width = cellWidth;
        height = (cellWidth * imageHeight) / imageWidth;

        load(gif.id);
    }

    public void onGifClick() {
        if(this.isAnimating) {
            listener.stopAnimation();
            this.isAnimating = false;
        }
        else
        {
            listener.startAnimation();
            this.isAnimating = true;
        }
    }

    public void load(String id) {
        final String idString = id;

        dataAccess.downloadGif(gif.images.fixed_width.url)
                .subscribe(bytes -> {
                    if (idString == this.gif.id && bytes != null) {
                        this.bytes.set(bytes);
                        this.visibility.set(View.VISIBLE);
                    }
                    else
                    {

                    }
                });
    }

    public interface Listener {


        void stopAnimation();

        void startAnimation();
    }
}
