package lv.id.arseniuss.gipher.di.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import javax.inject.Singleton;

import dagger.Component;
import lv.id.arseniuss.gipher.di.NetworkModule;

@Singleton
@Component(modules = { AppModule.class, NetworkModule.class})
public interface AppComponent {
    SharedPreferences sharedPreferences();
    Gson gson();
    Context getContext();

}
