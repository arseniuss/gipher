package lv.id.arseniuss.gipher.model.preference;

import rx.Observable;

public interface PreferenceHelper<T> {
    Observable<T> save(String key, T value);

    Observable<T> get(String key, Class<T> generic);

    Observable<Boolean> clear();
}
