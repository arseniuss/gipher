package lv.id.arseniuss.gipher.view;


import android.app.AlertDialog;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.logging.Logger;

import javax.inject.Inject;

import lv.id.arseniuss.gipher.GipherApplication;
import lv.id.arseniuss.gipher.R;
import lv.id.arseniuss.gipher.databinding.FragmentMainBinding;
import lv.id.arseniuss.gipher.di.MainScope;
import lv.id.arseniuss.gipher.model.service.entity.GifSearchResponse;
import lv.id.arseniuss.gipher.utils.EndlessRecyclerViewScrollListener;
import lv.id.arseniuss.gipher.viewmodel.MainViewModel;

import static android.content.res.Configuration.ORIENTATION_LANDSCAPE;
import static android.content.res.Configuration.ORIENTATION_PORTRAIT;

@MainScope
public class MainFragment extends Fragment implements MainViewModel.MainViewListener {
    private EndlessRecyclerViewScrollListener endlessRecyclerViewScrollListener;
    private static final String QUERY_TAG = "QUERY";
    private FragmentMainBinding binding;

    @Inject
    MainViewModel viewModel;

    @Inject
    GifAdapter adapter;

    public static MainFragment newInstance() {
        Bundle args = new Bundle();

        MainFragment fragment = new MainFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public MainFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false);

        initInject();
        initView();

        if (savedInstanceState != null) {
            String query = savedInstanceState.getString(QUERY_TAG);

            viewModel.setQuery(query);
        }

        return binding.getRoot();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        String query = viewModel.query.get();

        outState.putString(QUERY_TAG, query);
    }

    private void initInject() {
        ((GipherApplication) getActivity().getApplication())
                .getMainComponent()
                .inject(this);

        binding.setViewModel(viewModel);
        viewModel.setViewListener(this);
    }

    private void initView() {
        int displayMode = getResources().getConfiguration().orientation;
        StaggeredGridLayoutManager staggeredGridLayoutManager;

        if (displayMode == ORIENTATION_LANDSCAPE) {
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(3, OrientationHelper.HORIZONTAL);
        } else {
            staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, OrientationHelper.VERTICAL);
        }

        binding.recyclerView.setLayoutManager(staggeredGridLayoutManager);
        binding.recyclerView.setAdapter(adapter);
        endlessRecyclerViewScrollListener = new EndlessRecyclerViewScrollListener(staggeredGridLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                viewModel.loadMore(page);
            }
        };

        binding.recyclerView.addOnScrollListener(endlessRecyclerViewScrollListener);
    }


    @Override
    public void onGifsLoaded(GifSearchResponse response, Boolean isMore) {
        Log.d("DEBUG",String.format("offset: %s", response.pagination.offset));

        if (isMore) {
            adapter.addGifs(response.data);
        } else {
            adapter.setGifs(response.data);
        }
    }


    @Override
    public void onError(Throwable error) {
        new AlertDialog.Builder(this.getActivity())
                .setTitle(R.string.dialog_error_title)
                .setMessage(error.getMessage())
                .show();

    }

}
