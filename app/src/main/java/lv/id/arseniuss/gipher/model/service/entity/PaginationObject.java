package lv.id.arseniuss.gipher.model.service.entity;

public class PaginationObject {
    public int offset;
    public int total_count;
    public int count;
}
