package lv.id.arseniuss.gipher.di;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import dagger.Module;
import dagger.Provides;
import lv.id.arseniuss.gipher.BuildConfig;
import lv.id.arseniuss.gipher.model.preference.PreferenceHelper;
import lv.id.arseniuss.gipher.model.service.DataAccess;
import lv.id.arseniuss.gipher.model.service.DataAccessImpl;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class NetworkModule {

    @Provides
    HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();

        if(BuildConfig.DEBUG) {
            interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        else
        {
            interceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }

        return interceptor;
    }

    @Provides
    OkHttpClient provideOkHttpClient(HttpLoggingInterceptor interceptor) {
        List<Protocol> protocols = new ArrayList<>();

        protocols.add(Protocol.HTTP_1_1);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .connectTimeout(1, TimeUnit.MINUTES)
                .readTimeout(1, TimeUnit.MINUTES)
                .protocols(protocols)
                .addInterceptor(interceptor)
                .build();

        return  okHttpClient;
    }

    @Provides
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.giphy.com")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build();

        return retrofit;
    }

    @Provides
    DataAccess provideDataAccess(OkHttpClient httpClient, Retrofit retrofit, PreferenceHelper<String> stringPreferenceHelper) {
        return new DataAccessImpl(httpClient, retrofit, stringPreferenceHelper);
    }
}
