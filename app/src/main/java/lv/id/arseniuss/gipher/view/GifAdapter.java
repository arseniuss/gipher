package lv.id.arseniuss.gipher.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.recyclerview.extensions.ListAdapter;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import lv.id.arseniuss.gipher.databinding.ItemMainListBinding;
import lv.id.arseniuss.gipher.model.service.DataAccess;
import lv.id.arseniuss.gipher.model.service.entity.GifObject;
import lv.id.arseniuss.gipher.viewmodel.MainViewItemViewModel;

public class GifAdapter extends ListAdapter<GifObject, GifAdapter.GifViewHolder> {
    private List<GifObject> gifs = new ArrayList<>();
    private DataAccess dataAccess;
    private int cellWidth;

    public static final DiffUtil.ItemCallback<GifObject> diffCallback = new DiffUtil.ItemCallback<GifObject>() {
        @Override
        public boolean areItemsTheSame(GifObject oldItem, GifObject newItem) {
            return oldItem.id == newItem.id;
        }

        @Override
        public boolean areContentsTheSame(GifObject oldItem, GifObject newItem) {
            return oldItem.id == newItem.id;
        }
    };

    @Inject
    public GifAdapter(Context context, DataAccess dataAccess) {
        super(diffCallback);
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        this.dataAccess = dataAccess;
        this.cellWidth = displayMetrics.widthPixels / 2;
    }

    public void setGifs(List<GifObject> data) {
        gifs.clear();
        gifs = data;
        submitList(gifs);
    }

    public void addGifs(List<GifObject> data) {
        int pos = gifs.size()-1;

        gifs.addAll(data);
        notifyItemInserted(pos);
    }

    @NonNull
    @Override
    public GifViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return GifViewHolder.create(LayoutInflater.from(parent.getContext()), parent);
    }

    @Override
    public void onBindViewHolder(@NonNull GifViewHolder holder, int position) {
        holder.bind(new MainViewItemViewModel(dataAccess, holder, getItem(position), cellWidth));
    }

    static class GifViewHolder extends RecyclerView.ViewHolder implements MainViewItemViewModel.Listener {
        private final ItemMainListBinding binding;

        public static GifViewHolder create(LayoutInflater inflater, ViewGroup parent) {
            ItemMainListBinding binding = ItemMainListBinding.inflate(inflater, parent, false);

            return new GifAdapter.GifViewHolder(binding);
        }

        public GifViewHolder(ItemMainListBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(MainViewItemViewModel viewModel) {
            binding.setViewModel(viewModel);
            try {
                binding.executePendingBindings();
            } catch (Exception ex) {

            }
            binding.itemLayout.getLayoutParams().width = viewModel.width;
            binding.itemLayout.getLayoutParams().height = viewModel.height;
        }

        @Override
        public void stopAnimation() {
            binding.gifImageView.stopAnimation();
        }

        @Override
        public void startAnimation() {
            binding.gifImageView.startAnimation();
        }
    }
}