package lv.id.arseniuss.gipher.model.service.entity;

public class ImageObject {
    public String url;
    public String width;
    public String height;
    public String size;
    public String mp4;
    public String mp4_size;
    public String webp;
    public String webp_size;
}
