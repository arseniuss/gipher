package lv.id.arseniuss.gipher.di.main;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import dagger.Module;
import dagger.Provides;
import lv.id.arseniuss.gipher.di.MainScope;
import lv.id.arseniuss.gipher.di.NetworkModule;
import lv.id.arseniuss.gipher.model.preference.PreferenceHelper;
import lv.id.arseniuss.gipher.model.preference.PreferenceHelperImpl;
import lv.id.arseniuss.gipher.model.service.DataAccess;
import lv.id.arseniuss.gipher.view.GifAdapter;
import lv.id.arseniuss.gipher.viewmodel.MainViewModel;


@Module(includes = { NetworkModule.class })
public class MainModule {

    MainViewModel mainViewModel;

    public MainModule() {

    }

    @Provides
    GifAdapter provideGifAdapter(Context context, DataAccess dataAccess) {
        return new GifAdapter(context, dataAccess);
    }

    @MainScope
    @Provides
    MainViewModel provideMainViewModel(DataAccess dataAccess) {
        return new MainViewModel(dataAccess);
    }

    @Provides
    PreferenceHelper<String> provideStringPreferenceHelper(SharedPreferences sharedPreferences, Gson gson) {
        return new PreferenceHelperImpl<String>(sharedPreferences, gson);
    }
}
