package lv.id.arseniuss.gipher.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.paging.LivePagedListBuilder;
import android.arch.paging.PagedList;
import android.databinding.ObservableBoolean;
import android.databinding.ObservableField;
import android.util.Log;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;
import javax.inject.Singleton;

import lv.id.arseniuss.gipher.model.service.DataAccess;
import lv.id.arseniuss.gipher.model.service.entity.GifObject;
import lv.id.arseniuss.gipher.model.service.entity.GifSearchResponse;
import rx.Observable;
import rx.subjects.PublishSubject;

@Singleton
public class MainViewModel {
    public final int PAGE_SIZE = 20;

    public ObservableField<String> query = new ObservableField<>("");
    public ObservableBoolean hasGifs = new ObservableBoolean(false);

    private DataAccess dataAccess;
    private MainViewListener viewListener;
    private PublishSubject<String> publishSubject = PublishSubject.create();

    @Inject
    public MainViewModel(DataAccess dataAccess) {
        this.dataAccess = dataAccess;

        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(PAGE_SIZE)
                .build();
    }

    public void setViewListener(MainViewListener viewListener) {
        this.viewListener = viewListener;

        publishSubject
                .debounce(1, TimeUnit.SECONDS)
                .flatMap(s -> dataAccess.searchGif(s, PAGE_SIZE, 0))
                .subscribe(this::onGifsLoaded, viewListener::onError);
    }

    public void onGifsLoaded(GifSearchResponse response) {
        viewListener.onGifsLoaded(response, false);
        hasGifs.set(true);
    }

    public void onAddionalGifsLoaded(GifSearchResponse response) {
        viewListener.onGifsLoaded(response, true);
    }

    public void onQueryChanged(String query) {
        Log.d("DEBUG", "Text changed: " + query);

        this.query.set(query);

        if (query.trim().length() > 0) {
            publishSubject.onNext(query.trim());
        }
    }

    public void setQuery(String query) {
        this.query.set(query);

        this.onQueryChanged(query);
    }

    public void loadMore(int page) {
        Observable
                .fromCallable(() -> dataAccess.searchGif(query.get(), PAGE_SIZE, page))
                .flatMap(s -> s)
                .subscribe(this::onAddionalGifsLoaded, viewListener::onError);
    }

    public interface MainViewListener {
        void onGifsLoaded(GifSearchResponse response, Boolean isMore);

        void onError(Throwable error);
    }
}
