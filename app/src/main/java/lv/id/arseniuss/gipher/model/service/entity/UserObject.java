package lv.id.arseniuss.gipher.model.service.entity;

public class UserObject {
    public String avatar_url;
    public String banner_url;
    public String profile_url;
    public String username;
    public String display_name;
    public String twitter;
}
