package lv.id.arseniuss.gipher.di.main;

import dagger.Component;
import lv.id.arseniuss.gipher.di.MainScope;
import lv.id.arseniuss.gipher.di.NetworkModule;
import lv.id.arseniuss.gipher.di.app.AppComponent;
import lv.id.arseniuss.gipher.view.MainActivity;
import lv.id.arseniuss.gipher.view.MainFragment;

@MainScope
@Component(dependencies =  {AppComponent.class }, modules = { NetworkModule.class,  MainModule.class})
public interface MainComponent {
    void inject(MainFragment fragment);
    void inject(MainActivity activity);
}
