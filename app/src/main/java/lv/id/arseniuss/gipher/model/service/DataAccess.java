package lv.id.arseniuss.gipher.model.service;

import lv.id.arseniuss.gipher.model.service.entity.GifSearchResponse;
import rx.Observable;

public interface DataAccess {
    Observable<GifSearchResponse> searchGif(String query, int limit, int offset);

    Observable<byte[]> downloadGif(String url);
}
