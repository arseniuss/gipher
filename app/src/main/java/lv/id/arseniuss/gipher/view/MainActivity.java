package lv.id.arseniuss.gipher.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import javax.inject.Inject;

import lv.id.arseniuss.gipher.GipherApplication;
import lv.id.arseniuss.gipher.R;
import lv.id.arseniuss.gipher.databinding.ActivityMainBinding;
import lv.id.arseniuss.gipher.di.MainScope;
import lv.id.arseniuss.gipher.di.main.MainComponent;
import lv.id.arseniuss.gipher.viewmodel.MainViewModel;

@MainScope
public class MainActivity extends AppCompatActivity {
    @Inject
    MainViewModel viewModel;

    MainComponent mainComponent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        MainFragment mainFragment = MainFragment.newInstance();

        initInject();
        initView(mainFragment);
    }

    private void initInject() {
        ((GipherApplication)getApplication())
                .getMainComponent()
                .inject(this);
    }

    private void initView(MainFragment mainFragment) {
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        binding.setViewModel(viewModel);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.mainFragment, mainFragment)
                .commitAllowingStateLoss();
    }
}
