package lv.id.arseniuss.gipher.model.service.entity;

import java.util.List;

public class GifSearchResponse {
    public List<GifObject> data;

    public PaginationObject pagination;

    public MetaObject meta;
}